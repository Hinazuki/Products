<?php

use Illuminate\Http\Request;
use App\Task;
use App\Project;
use App\Comment;
use App\User;
//use Illuminate\Support\Facades\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/user/register',function(Request $request){
    $data = $request->only(['name','email','password']);
    $validator=validator($data,
        [   
            'name'=>'required',
            'email'=>'required',
            'password'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                'errors'=>$validator->errors()
            ],409);
    }
    $user = new \App\User();
    $user -> name =$data['name'];
    $user -> email = $data['email'];
    $user -> password = md5($data['password']);
    $user -> api_token = str_random(60);
    $user ->save();
    return response($user,201);
 });
 Route::post('/user',function(Request $request){
    $data = $request->only(['email','password']);
    $validator=validator($data,
        [   
            'email'=>'required',
            'password'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                'errors'=>$validator->errors()
            ],409);
    }
    $user = User::where("email",$data["email"])->where("password",md5($data["password"]))->get()->first();
    return response($user,201);
  });
  Route::get('user/project/completed/{id}',function($id,Request $request){
    $Project=Project::where("status","completed")->where("id_creator","".$id)->get();
     if ($Project){
        return $Project; 
     }
     else{
         return response(
             [
                 'Message'=>'ressource not found'
             ],404);
     } 
  });
  Route::get('user/project/inprogress/{id}',function($id,Request $request){
    $Project=Project::where("status","in progress")->where("id_creator","".$id)->get();
     if ($Project){
        return $Project; 
     }
     else{
         return response(
             [
                 'Message'=>'ressource not found'
             ],404);
     } 
  });
Route::get('/project',function(Request $request){

    $user = \Auth::user();

    
    if ($Project->isNotEmpty()){
       return $Project; 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    }    
 })->middleware('auth:api');

 Route::get('/project/completed',function(Request $request){
    $Project=Project::all()->where("status","completed")->toArray();
     if ($Project){
        return array_values($Project); 
     }
     else{
         return response(
             [
                 'Message'=>'ressource not found'
             ],404);
     } 
  });
  Route::get('/project/inprogress',function(Request $request){
    $Project=Project::all()->where("status","in progress")->toArray();
    if ($Project){
       return array_values($Project); 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    } 
 });
Route::get('/project/task',function(Request $request){
    $Task=Task::all();
    if ($Task->isNotEmpty()){
       return $Task; 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    }    
 });
 Route::post('/project',function(Request $request){
    $data = $request->only(['id_creator','Title','description','status','priority']);
    $validator=validator($data,
        [   
            'id_creator'=>'required',
            'Title'=>'required',
            'description'=>'required',
            'status'=>'required',
            'priority'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                'errors'=>$validator->errors()
            ],409);
    }
    $Project=Project::create($data);
    return response($Project,201);
 });
 Route::patch('/project/{id}',function($id,Request $request){
    $Project=Project::find($id);
    $data = $request->only(['Title','description','status','priority']);
    $validator=validator($data,
        [
            'Title'=>'required',
            'description'=>'required',
            'status'=>'required',
            'priority'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                $validator->errors()
            ],409);
    }
    $Project->fill($data);
    if($Project->save()){
        return response($Project,201);
    }   
 });
 Route::delete('/project/{id}',function($id){
    $Project=Project::find($id);
    if($Project!=null){
        $Project->delete();
        return response(
            [
                'Message'=>'objet n°'.$id.' supprimé',
                'objet'=>$Project
            ],201);
    }
    else{
        return response(
            [
                'Message'=>'objet n°'.$id.' introuvable'
            ],404);
    }
});
 Route::get('/project/{id}',function($id){
    $Project=Project::find($id);
    if ($Project!=null){
       return $Project; 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    }
});
 Route::get('/project/task/completed/{id}',function(Request $request,$id){
   $Task=Task::all()->where("status","completed")->where("id_project","".$id)->toArray();
    if ($Task){
       return array_values($Task); 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    } 
 });
 Route::get('/project/task/inprogress/{id}',function(Request $request,$id){
    $Task=Task::all()->where("status","in progress")->where("id_project","".$id)->toArray();
    if ($Task){
       return array_values($Task); 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    } 
 });
 Route::get('/project/task/{id}',function($id){
     $Task=Task::find($id);
     if ($Task!=null){
        return $Task; 
     }
     else{
         return response(
             [
                 'Message'=>'ressource not found'
             ],404);
     }
 });
 Route::post('/project/task',function(Request $request){
    $data = $request->only(['id_creator','id_project','Title','description','status','priority']);
    $validator=validator($data,
        [   
            'id_creator'=>'required',
            'id_project'=>'required',
            'Title'=>'required',
            'description'=>'required',
            'status'=>'required',
            'priority'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                'errors'=>$validator->errors()
            ],409);
    }
    $Task=Task::create($data);
    return response($Task,201);
 });
 Route::post('/project/task/comment',function(Request $request){
    $data = $request->only(['id_Task','Comment']);
    $validator=validator($data,
        [   
            'id_Task'=>'required',
            'Comment'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                'errors'=>$validator->errors()
            ],409);
    }
    $Comment=Comment::create($data);
    return response($Comment,201);
 });
 Route::get('/project/task/comment/{id}',function($id){
    $comment=Comment::all()->where("id_Task",$id)->toArray();
    if ($comment){
       return array_values($comment); 
    }
    else{
        return response(
            [
                'Message'=>'ressource not found'
            ],404);
    }
});
 Route::patch('/project/task/{id}',function($id,Request $request){
    $Task=Task::find($id);
    $data = $request->only(['id_user','Title','description','Comment','status','priority']);
    $validator=validator($data,
        [
            'Title'=>'required',
            'description'=>'required',
            'status'=>'required',
            'priority'=>'required'
        ]
        );
    if($validator->fails()){
        return response(
            [
                $validator->errors()
            ],409);
    }
    $Task->fill($data);
    if($Task->save()){
        return response($Task,201);
    }   
 });
Route::delete('/project/task/{id}',function($id){
    $Task=Task::find($id);
    if($Task!=null){
        $Task->delete();
        return response(
            [
                'Message'=>'objet n°'.$id.' supprimé',
                'objet'=>$Task
            ],201);
    }
    else{
        return response(
            [
                'Message'=>'objet n°'.$id.' introuvable'
            ],404);
    }
});
