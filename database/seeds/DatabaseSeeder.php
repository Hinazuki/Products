<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Projects')->insert([
            'id_creator'=>'2',
            'Title'=>'Do somethings',
            'description'=>'i have to do somethings',
            'status'=>'in progress',
            'priority'=>'Medium'
        ]);
        DB::table('Projects')->insert([
            'id_creator'=>'1',
            'Title'=>'Do somethings',
            'description'=>'i have to do somethings',
            'status'=>'in progress',
            'priority'=>'Medium'
        ]);
        DB::table('tasks')->insert([
            'id_creator'=>'2',
            'id_user'=>'Morel',
            'id_project'=>'1',
            'Title'=>'Do somethings',
            'description'=>'i have to do somethings',
            'status'=>'in progress',
            'priority'=>'Medium'
        ]);
        DB::table('tasks')->insert([
            'id_creator'=>'2',
            'id_user'=>'Dinahet',
            'id_project'=>'1',
            'Title'=>'somethings',
            'description'=>' somethings',
            'status'=>'completed',
            'priority'=>'Low'
        ]);
        DB::table('tasks')->insert([
            'id_creator'=>'1',
            'id_user'=>'Bourhis',
            'id_project'=>'2',
            'Title'=>'Do',
            'description'=>'i have',
            'status'=>'in progress',
            'priority'=>'Hight'
        ]);
        DB::table('Comments')->insert([
            'id_Task'=>'1',
            'Comment'=>'Blabla blabla blabla 1',
        ]); 
        DB::table('Comments')->insert([
            'id_Task'=>'2',
            'Comment'=>'Blabla blabla blabla 2',
        ]);  
        DB::table('Comments')->insert([
            'id_Task'=>'3',
            'Comment'=>'Blabla blabla blabla 3',
        ]);
        $user = new \App\User();
        $user -> name ='Tristan';
        $user -> email = 'Tristan@hotmail.fr';
        $user -> password = md5('123456');
        $user -> api_token = str_random(60);
        $user ->save();

        $user = new \App\User();
        $user -> name ='Toffer';
        $user -> email = 'Toffer@hotmail.fr';
        $user -> password = md5('123456');
        $user -> api_token = str_random(60);
        $user ->save();
    }
}
