<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable=['id_creator','id_user','id_project','Title','description','comment','status','priority'];
}

